package main

import (
	"fmt"
	"time"

	"github.com/getsentry/sentry-go"
)

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:   "https://bb7eba91b5c147ec97848ee23bce6f84@f37628959f94.eu.ngrok.io/2",
		Debug: true,
	})
	if err != nil {
		panic(err)
	}
	defer sentry.Flush(2 * time.Second)
	sentry.CaptureException(fmt.Errorf("test error another"))
}
